
var root = document.querySelector(':root')
var container = document.querySelector('.container');
var newTaskInput = document.getElementById('new_task_input')
var taskform = document.getElementById('new_task_form');
var tasksList = document.getElementById('tasksList');
var taskBtns = document.querySelectorAll('.task_check_btn');
var themeBtn = document.querySelector('.theme_toogle_btn');

taskform.addEventListener('submit', function (e) {

    e.preventDefault();
    var newtaskInputValue = taskform.elements.new_task_input;


    addTask(newtaskInputValue.value)

    newtaskInputValue.value = '';
    container.classList.remove('task_list_empty');
})

function countTasks(){
    document.getElementById('countToDos').innerHTML = tasksList.childElementCount;
}

function addTask(newTask) {

    if ("" === newTaskInput.value) {
        alert('Task Input is Empty, Consider adding some text :)');
    } else {

        const newTaskItem = document.createElement('li');
        newTaskItem.setAttribute('class', 'task_item');
    
    
        const newCheckBtn = document.createElement("input");
        newCheckBtn.setAttribute('type', 'checkbox')
        newCheckBtn.setAttribute('name', 'checkbox')
        newCheckBtn.setAttribute('onchange', 'taskChange(event)')
        newCheckBtn.setAttribute('class', 'task_check_btn')
    
        const newTaskBio = document.createElement('span');
        newTaskBio.setAttribute('class', 'task_bio')

        newTaskBio.innerText = newTask;
        
       
        tasksList.appendChild(newTaskItem)
    
        newTaskItem.appendChild(newCheckBtn)
    
        newTaskItem.appendChild(newTaskBio)
    
        onTaskComplete(newCheckBtn)


        countTasks()
        

    }
}

function onTaskComplete(btns) {

    btns.addEventListener('click', function (e) {
        var parent = e.target.parentElement;
        parent.classList.add('task-completed'); 
        e.target.offsetParent.children[1].classList.add('task-bio-completed')
        const newTaskDeleteButton = document.createElement('button');
        newTaskDeleteButton.setAttribute('class', 'task_delete')
        newTaskDeleteButton.setAttribute('onclick', 'deleteTask(event)')
        newTaskDeleteButton.innerText = "Delete"
        parent.appendChild(newTaskDeleteButton)

    })

}

function taskChange(event) {
  if (!event.target.checked) {
    event.target.parentElement.classList.remove('task-completed');
    event.target.offsetParent.children[1].classList.remove('task-bio-completed')
    event.target.parentNode.removeChild(event.target.offsetParent.children[2])
    event.target.offsetParent.children[2].remove();
  }
}

function deleteTask(event) {
    event.target.parentElement.remove()
    countTasks()
}



